export default {
    projects: [
        {
            title: 'Building Washrooms',
            slug: 'building-washrooms',
            image: '/assets/Toilet.jpg',
            content: `<p>Already they have built over 30 toilets in three schools, rehabilitated several classrooms, dug boreholes and paid school fees for several students.Yet, even before the setting up of the Foundation, the couple has been paying school fees for needy children for long now. Courtesy of their generosity, a number of needy students have already graduated from university and close to 20 others are in secondary schools, tertiary institutions and university. “We, as a company, were keen on positively impacting on the lives of not only just our relatives, but also society as a whole’’ she explained. It is a task, they felt, needed a structured way of implementing rather than being carried out in an adhoc manner.</p>`
        },
        {
            title: 'Establishing community support systems',
            slug: 'digging-boreholes',
            image: '/assets/DSC_6778.1.jpg',
            content: `<p>“We envision a healthy society across the country because most of the diseases are easily preventable with a simple hygiene. This will no doubt help in improving education standards in the long run,” she concluded.</p>`
        },
        {
            title: 'Establishing support systems',
            slug: 'support-systems',
            image: '/assets/projo.jpg',
            content: `<p>And Suraya Property Group, a Real Estate company incorporated in 2006 by Kenyan entrepreneurs Peter and Sue Muraya, has proven itself to be a good and responsible corporate citizen by making great strides in giving back to society.Under a careful and well thought-out corporate social responsibility (CSR) programme, the fast-growing company has managed to implement initiatives aimed at benefiting society both in the short- and long-term.</p>`
        },
        {
            title: 'Supporting Education',
            slug: 'supporting-eductaion',
            image: '/assets/Baba_Dogo_Catholic_Primary_41.jpg',
            content: `<p>Some of the chairs and tables donated by The Suraya Foundation at Baba Dogo Sacred – Catholic Church- Primary School.</p>`
        },
        {
            title: 'Showcasing and supporting talent',
            slug: 'talent',
            image: '/assets/mat.jpg',
            content: '<p>We provide a platform for upcoming artists in arts and design to showcase their talent.</p>'
        }
    ]
}